# Yoda-Quotes
**Yoda-Quotes** is a simple app for Fitbit OS that generates a pseudo-random quote from an array of 45 Yoda quotes. This app uses [Fitbit SDK V3.1.0](https://github.com/Fitbit).

**Yoda-Quotes** is available for Install from the FitBit App Gallery: [Yoda-Quotes](https://gallery.fitbit.com/details/001f0feb-9bf0-49db-88ed-10010b4e862b).

## Devices
**Yoda-Quotes** is built for Fitbit Devices: Ionic (348x250), Versa (300x300) and Versa Lite (300x300):

## Attribution
**Yoda-Quotes** Image [Yoda-star-wars-jedi-she-makes-1091030](https://pixabay.com/illustrations/yoda-star-wars-jedi-she-makes-1091030/) Attribution [Comfreak](https://pixabay.com/users/comfreak-51581/) | Simplified [Pixabay License](https://pixabay.com/service/license/).

## Build
* Pre-Requisite: [Node.js 8](https://nodejs.org/en/download/) Installed.
* See FitBit OS Command Line Interface Tool: [CLI Tool Guide](https://dev.fitbit.com/build/guides/command-line-interface/).

```console
npm install
npx fitbit-build
npx fitbit
fitbit$ install
fitbit$ build
```
## License
**Yoda-Quotes** App is licensed under the terms of the [GPL-3.0 License](/LICENSE). 
